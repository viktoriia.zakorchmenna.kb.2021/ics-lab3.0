#!/bin/bash


target_dir="$1"

mkdir -p "$target_dir/documents"
mkdir -p "$target_dir/images"
mkdir -p "$target_dir/other"


for file in "$target_dir"/*; do
    if [ -f "$file" ]; then
        ext="${file##*.}"
        case "$ext" in
            doc|pdf|txt)
                mv "$file" "$target_dir/documents/"
                ;;

  jpg|png|gif)
                mv "$file" "$target_dir/images/"
                ;;
            *)
                mv "$file" "$target_dir/other/"
                ;;
        esac
    fi
done


doc_count=$(find "$target_dir/documents" -type f | wc -l)
img_count=$(find "$target_dir/images" -type f | wc -l)
other_count=$(find "$target_dir/other" -type f | wc -l)

echo "Переміщено $doc_count файлів у $target_dir/documents"
echo "Переміщено $img_count файлів у $target_dir/images"
echo "Переміщено $other_count файлів у $target_dir/other"h e l l o  
 s m i l e  
 